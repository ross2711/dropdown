import { Directive, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[defFocus]'
})
export class DefFocusDirective implements AfterViewInit {
  constructor(private el: ElementRef) {}

  ngAfterViewInit() {
    console.log('rendered 😁');
    console.log(this.el.nativeElement.focus());
  }

  setFocus() {
    this.el.nativeElement.focus();
  }
}
